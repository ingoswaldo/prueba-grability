<?php

namespace App;


abstract class Heroes
{
    /**
     * @var array
     */
    public $ruta;

    /**
     * @var integer
     */
    public $tiempo;

    abstract protected function tiempoEnRuta($tiempos);

    public function __construct()
    {
        $this->ruta = [1];
        $this->tiempo = 0;
    }
}