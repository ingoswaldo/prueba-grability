<?php

namespace App\Http\Controllers;

use App\Archivo;
use App\BigCat;
use App\Bitville;
use App\LittleCat;
use Illuminate\Http\Request;

class ArchivoController extends Controller
{
    public function obtenerTiempo(Request $request)
    {
        //se verifica si se manda en el formulario el archivo
        if ($request->hasFile('file')){
            $archivo = new Archivo($request->file('file')->getRealPath());
            $bitville = new Bitville($archivo->getCantidadCentrosComerciales(), $archivo->getCantidadCalles(), $archivo->getCantidadTiposPescados());
            $distanciasCalles = $archivo->getDistanciaCallesEnArchivo($bitville->cantidadCentrosComerciales);
            $bigCat = new BigCat();
            $littleCat = new LittleCat();

            if (isset($distanciasCalles)){
                $cantidadDistancias = count($distanciasCalles);
                //se recorren todas las distancias
                foreach ($distanciasCalles as $key => $distancia) {

                    if ($key == 0){

                        if ($distancia[0] != 1 && $distanciasCalles[0][1] != 1) return response()->json(['success' => false]);

                    }elseif ($key + 1 == $cantidadDistancias){

                        if ($distancia[1] != $distanciasCalles[$key - 1][1]) return response()->json(['success' => false]);
                    }

                    if ($key % 2 == 0){
                        $bigCat->ruta[] = $distancia[1];
                        $bigCat->tiempo += $distancia[2];

                    } else{
                        $littleCat->ruta[] = $distancia[1];
                        $littleCat->tiempo += $distancia[2];
                    }

                }
                //se verifica cual de los dos heroes llega primero
                if ($cantidadDistancias % 2 == 0){
                    $tiempo = $bigCat->tiempo + abs($bigCat->tiempo - $littleCat->tiempo);

                } else $tiempo = $littleCat->tiempo + abs($littleCat->tiempo - $bigCat->tiempo);

                return response()->json(['success' => true, 'tiempo' => $tiempo]);

            } else return response()->json(['success' => false]);
        }
        return response()->json(['success' => false]);
    }
}
