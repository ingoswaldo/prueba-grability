<?php

namespace App;


class BigCat extends Heroes
{
    /**
     * BigCat constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Obtiene el tiempo que se demora un heroe en recorrer la ruta
     * @param array $tiempos
     * @return integer
     */
    protected function tiempoEnRuta($tiempos)
    {
        return 0;
    }
}