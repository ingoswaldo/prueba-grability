<?php

namespace App;


class Bitville
{
    public $cantidadCentrosComerciales = 0;
    public $cantidadCalles = 0;
    public $cantidadTiposPescados = 0;

    public function __construct($cantidadCentrosComerciales, $cantidadCalles, $cantidadTiposPescados)
    {
        $this->cantidadCentrosComerciales = $cantidadCentrosComerciales;
        $this->cantidadCalles = $cantidadCalles;
        $this->cantidadTiposPescados = $cantidadTiposPescados;
    }
}