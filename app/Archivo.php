<?php

namespace App;


use Illuminate\Support\Collection;

class Archivo
{
    //se definen los valores constantes para cada atributo especifico
    const cantidadMinimaCentroComerciales = 2;
    const cantidadMaximaCentrosComerciales = 10000;
    const cantidadMinimaCalles = 1;
    const cantidadMaximaCalles = 20000;
    const cantidadMinimaTiposPescado = 1;
    const cantidadMaximaTiposPescado = 10;
    const valorMaximoDistancia = 100000;

    //variable privada para obtener el contendio del archivo txt
    private $contenido;

    /**
     * Archivo constructor.
     * @param $rutaArchivo
     */
    public function __construct($rutaArchivo)
    {
        $this->contenido = file($rutaArchivo);
    }

    /**
     * Obtiene la cantidad de centros comerciales escritos en el archivo enviado
     * @return integer
     */
    public function getCantidadCentrosComerciales()
    {
        if($this->validarPrimeraLineaArchivo()){
            $cantidad = $this->contenido[0][0];

            if ($this->validarCantidadCentrosComerciales($cantidad)) return $cantidad;

            return 0;

        }
        return 0;
    }

    /**
     * Obtiene la cantidad de calles escritas en el archivo
     * @return int
     */
    public function getCantidadCalles()
    {
        if($this->validarPrimeraLineaArchivo()){
            $cantidad = $this->contenido[0][2];

            if ($this->validarCantidadCalles($cantidad)) return $cantidad;

            return 0;
        }
        return 0;
    }

    /**
     * Obtiene la cantidad de tipos de pescados escritos en el archivo
     * @return int
     */
    public function getCantidadTiposPescados()
    {
        if($this->validarPrimeraLineaArchivo()){
            $cantidad = $this->contenido[0][4];

            if ($this->validarCantidadTiposPescado($cantidad)) return $cantidad;

            return 0;
        }

        return 0;
    }

    /**
     * obtiene una coleccion con la configuracion que tiene los tipos de pescado por vendedor por centro comercial
     * @param integer $cantidadCentrosComerciales
     * @param integer $cantidadTiposPescados
     * @return Collection|null
     */
    public function getTiposPescadoEnArchivo($cantidadCentrosComerciales, $cantidadTiposPescados)
    {
        //coleccion de la configuracion de tipos de pescado a retornar
        $configuracion = collect();
        //se recorre la cantidad de centros comerciales, se evaluan las condiciones y se agregan los valores de connfiguracion
        for ($i = 1; $i <= $cantidadCentrosComerciales; $i++){
            $lineaTiposPescados = $this->stringToArraySinEspacios($this->contenido[$i]);
            $valores = []; // array que almacenara los valores para agregar en la coleccion
            //se recorren los valores de la linea i del archivo
            foreach ($lineaTiposPescados as $key => $item) {
                if ($key == 0){

                    if ($item >= 0 && $item <= $cantidadTiposPescados) $valores[] = $item;

                } else{

                    if ($item >= 1 && $item <= $cantidadTiposPescados) $valores[] = $item;
                }
            }

            $configuracion->push($valores);
        }
        //se verifica que todos los centros comerciales tengan vendedores
        if ($configuracion->count() == $cantidadCentrosComerciales){
            return $configuracion;
        }

        return null;
    }

    public function getDistanciaCallesEnArchivo($cantidadCentrosComerciales)
    {
        $configuracion = collect();
        //se recorre el archivo a partir del valor correspondiente de las distancias entre cada centro comercial
        for ($i = ($cantidadCentrosComerciales + 1); $i < count($this->contenido); $i++){
            $lineaDistancia = $this->stringToArraySinEspacios($this->contenido[$i]);
            $valores = [];
            //se verifica que la linea cumple con el formato correcto
            if (count($lineaDistancia) == 3){

                if ($this->validarDistancias($lineaDistancia, $cantidadCentrosComerciales)){
                    $valores[] = $lineaDistancia[0];
                    $valores[] = $lineaDistancia[1];
                    $valores[] = $lineaDistancia[2];

                } else return null;

            } else return null;

            $configuracion->push($valores);
        }

        return $configuracion;
    }

    /**
     * Verifica si la primera linea del archivo es valida para procesar
     * @return bool
     */
    private function validarPrimeraLineaArchivo()
    {
        //se verifica la cantidad de string en la primera linea del archivo
        if (strlen($this->contenido[0]) > 5) return true; //vaor retornado si cumple

        return false; //valor retornado si no cumple
    }

    /**
     * Valida la cantidad de centros comerciales obtenidos
     * @param integer $cantidad
     * @return bool
     */
    private function validarCantidadCentrosComerciales($cantidad)
    {
        //se verifica que cumpla con la condicion
        if ($cantidad >= self::cantidadMinimaCentroComerciales && $cantidad <= self::cantidadMaximaCentrosComerciales)
            return true; //valor retornado si cumple

        return false; //valor retornado si no cumple
    }

    /**
     * Valida la cantidad de caller obtenidos
     * @param integer $cantidad
     * @return bool
     */
    private function validarCantidadCalles($cantidad)
    {
        //se verifica que cumpla con la condicion
        if ($cantidad >= self::cantidadMinimaCalles && $cantidad <= self::cantidadMaximaCalles)
            return true; //valor retornado si cumple

        return false; //valor retornado si no cumple
    }

    /**
     * Valida la cantidad de centros comerciales obtenidos
     * @param integer $cantidad
     * @return bool
     */
    private function validarCantidadTiposPescado($cantidad)
    {
        //se verifica que cumpla con la condicion
        if ($cantidad >= self::cantidadMinimaTiposPescado && $cantidad <= self::cantidadMaximaTiposPescado)
            return true; //valor retornado si cumple

        return false; //valor retornado si no cumple
    }

    /**
     * convierte una cadena de texto a array quitandole los espacion en blanco y saltos de linea
     * @param string $texto
     * @return array
     */
    private function stringToArraySinEspacios($texto){
        return explode(' ', rtrim($texto));
    }

    /**
     * verifica los valores de las distancias dados en el archivo
     * @param array $distancia
     * @param integer $cantidadCentrosComerciales
     * @return bool
     */
    private function validarDistancias($distancia, $cantidadCentrosComerciales)
    {
        $valido = false;

        if ($distancia[0] >= 1 && $distancia[0] <= $cantidadCentrosComerciales){

            if ($distancia[1] >= 1 && $distancia[1] <= $cantidadCentrosComerciales){

                if ($distancia[2] >= 1 && $distancia[2] <= self::valorMaximoDistancia) $valido = true;
            }
        }

        return $valido;
    }
}