<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bitville</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head>
    <body class="body">
        <div class="container">
            <div class="d-flex align-items-center">
                <div class="offset-4">
                    <div class="card">
                        <div class="card-header">
                            BITVILLE
                        </div>
                        <div class="card-body">
                            <form id="form" action="{{ url('api/archivo/obtener-tiempo') }}" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="file">Archivo (txt)</label>
                                    <input type="file" class="form-control-file" id="file" name="file" accept="text/plain">
                                </div>
                                <button id="procesar" type="submit" class="btn btn-primary">Procesar</button>
                            </form>
                        </div>
                    </div>
                    <div id="alert-success" class="alert alert-success alert-dismissible fade" role="alert">
                        <h5>Para recolectar todos los tipos de pescados demora <strong id="tiempo"></strong></h5>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="alert-danger" class="alert alert-danger alert-dismissible fade" role="alert">
                        <h5><strong>Error!</strong> No se puede obtener el tiempo</h5>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $('#procesar').click(function (evento) {
                evento.preventDefault();

                let form = $('form');
                let data = new FormData(form[0]);

                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (respuesta) {
                        if (respuesta.success){
                            $('#tiempo').text(respuesta.tiempo);
                            $('#alert-success').addClass('show');

                        } else $('#alert-danger').addClass('show');
                    }
                });
            });

            $('button.close').click(function (evento) {
                evento.stopPropagation();
                $(this).parent().removeClass('show');
            });
        </script>
    </body>
</html>
